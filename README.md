#Handlebars Training Course 

This repo is for all the code files that pertain to all 16 videos in my Handlebars Youtube training course. I have to admit, it was a second thought to add these code files to Bitbucket, so things might not correspond 1-to-1 to the videos, and the code files might not match the videos exactly (sorry). This repo isn't in the best shape, but if you're watching the Youtube videos and want to use the files this repo will help. 

##https://www.youtube.com/playlist?list=PLtV5RF44Yj8S4RcpQehL-2XMuVsJXwNvK